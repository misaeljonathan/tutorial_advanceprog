package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void setFlyBehavior(FlyBehavior newOne) {
        this.flyBehavior = newOne;
    }

    public void setQuackBehavior(QuackBehavior newOne) {
        this.quackBehavior = newOne;
    }
    
    public void swim() {
        System.out.println("All ducks float, even decoys!");
    }
    
    public abstract void display();
}
