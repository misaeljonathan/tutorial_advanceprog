import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class PrimeCheckerTest {

    private static final List<Integer> PRIME_NUMBERS = Arrays.asList(2, 3, 5, 7);
    private static final List<Integer> NOTPRIME_NUMBERS = Arrays.asList(1, 4, 6, 8);

    @Test
    public void testIsPrimeFalseGivenNonPrimeNumbers() {
        NOTPRIME_NUMBERS.forEach(number -> assertFalse(PrimeChecker.isPrime(number)));
        // Given non-prime numbers
        // When isPrime is invoked
        // It should return false
    }
    
    @Test
    public void testIsPrimeTrueGivenPrimeNumbers() {
        PRIME_NUMBERS.forEach(number -> assertTrue(PrimeChecker.isPrime(number)));
    }
}