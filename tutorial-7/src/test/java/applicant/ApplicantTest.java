package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    private Applicant applicant;
    private Predicate<Applicant> qualifiedEvaluator;
    private Predicate<Applicant> creditEvaluator;
    private Predicate<Applicant> employmentEvaluator;
    private Predicate<Applicant> crimeEvaluator;


    @Before
    public void setUp() {
        applicant = new Applicant();
        qualifiedEvaluator = anApplicant -> anApplicant.isCredible();
        creditEvaluator = anApplicant -> anApplicant
                .getCreditScore() > 600;
        employmentEvaluator = anApplicant -> anApplicant
                .getEmploymentYears() > 0;
        crimeEvaluator = anApplicant -> !anApplicant
                .hasCriminalRecord();
    }

    @Test
    public void testQualifiedEvaluator() {
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator));
    }

    @Test
    public void testCreditEvaluator() {
        assertTrue(Applicant.evaluate(applicant, creditEvaluator));
    }

    @Test
    public void testEmploymentEvaluator() {
        assertTrue(Applicant.evaluate(applicant, employmentEvaluator));
    }

    @Test
    public void testCrimeEvaluator() {
        assertFalse(Applicant.evaluate(applicant, crimeEvaluator));
    }

    @Test
    public void testMultipleEvaluator() {
        boolean result = Applicant.evaluate(applicant, qualifiedEvaluator
                .and(employmentEvaluator)
                .and(creditEvaluator)
                .and(crimeEvaluator));
        assertFalse(result);
    }
}
