package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class PlainSauce implements Sauce {
    public String toString() {
        return "Marinara Sauce";
    }
}
