package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class TripleCheese implements Cheese {

    public String toString() {
        return "Triple Cheese";
    }
}
