package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class JumboMumboDough implements Dough {
    public String toString() {
        return "The Super Jumbo Mambo Jambo Dough!";
    }
}
