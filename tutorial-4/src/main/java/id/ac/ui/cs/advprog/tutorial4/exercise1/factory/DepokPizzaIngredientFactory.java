package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.TripleCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.TimmyTheClam;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.JumboMumboDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlainSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Cactus;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RafflesiaArnoldi;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new JumboMumboDough();
    }

    public Sauce createSauce() {
        return new PlainSauce();
    }

    public Cheese createCheese() {
        return new TripleCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Cactus(), new Onion(), new Mushroom(), new RafflesiaArnoldi()};
        return veggies;
    }

    public Clams createClam() {
        return new TimmyTheClam();
    }
}
