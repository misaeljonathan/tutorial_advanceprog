package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        System.out.println("Ethan goes to New York!");
        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");


        PizzaStore depokStore = new DepokPizzaStore();

        System.out.println("Ethan goes to Depok!");
        Pizza pizza2 = depokStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza2 + "\n");

        pizza2 = depokStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza2 + "\n");

        pizza2 = depokStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza2 + "\n");

    }
}
