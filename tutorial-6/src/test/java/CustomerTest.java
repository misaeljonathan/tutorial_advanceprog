import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    private Customer customer;
    private Movie movie;
    private Rental rent;
    private String result;

    @Before // setup()
    public void setUp() throws Exception {
        this.customer = new Customer("Alice");
        this.movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        this.rent = new Rental(movie, 3);
        this.result = "";
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        assertEquals(4, countResultLinesLength());
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent);
        customer.addRental(rent);

        assertEquals(6, countResultLinesLength());
        assertTrue(result.contains("Amount owed is 10.5"));
        assertTrue(result.contains("3 frequent renter points"));
    }

    @Test
    public void HtmlStatementWithSingleMovie() {
        customer.addRental(rent);

        assertEquals(4, countResultLinesLengthHTML());
        assertTrue(result.contains("<P>You owe <EM>"+"3.5"+"</EM><P>"));
        assertTrue(result.contains("On this rental you earned <EM>"+"1"+"</EM> frequent renter points<P>"));
    }

    @Test
    public void HtmlStatementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent);
        customer.addRental(rent);

        assertEquals(6, countResultLinesLengthHTML());
        assertTrue(result.contains("<P>You owe <EM>"+"10.5"+"</EM><P>"));
        assertTrue(result.contains("On this rental you earned <EM>"+"3"+"</EM> frequent renter points<P>"));
    }

    public int countResultLinesLength() {
        result = customer.statement();
        String[] lines = result.split("\n");
        return lines.length;
    }

    public int countResultLinesLengthHTML() {
        result = customer.htmlStatement();
        String[] lines = result.split("\n");
        return lines.length;
    }


}